# What
Toolkit which registers your application with the Kei Jidosha service registry

# How
* Add a kei.cfg and version.info to your application root
* Add env variable KEI_HOST, point to your installation of Key Jidosha, ie. http://localhost:1234

## kei.cfg
```
{
  "service": {
    "id": "string",
    "name": "string [optional]"
  },
  "dnsName": "string [optional]",
  "applicationName": "string",
  "input": {
    "type":"string:type (http|rabbitmq|redis|...)",
    "subscribesTo": "string [optional] (exchange connected to queue)",
    "href": "string (http endpoint link || queue connectionstring)"
  },
  "output": {
    "type":"string:type (http|rabbitmq|redis|...)",
    "href": "string"
  }
}```

## version.info
echo 1.0.0-yourVersionNumber > version.info
This is convenient to put in your build process to get your actual build number in there

const cfgLocator = require("./modules/cfg-locator")
const cfgFactory = require("./cfg-factory")
const request = require("request-promise-native")
const uuid = require("./modules/uuid")

if (!process.env.KEI_HOST) {
  console.error("No kei host specified, unable to register service application.")
  return
}

const _json = JSON.parse(cfgLocator.tryGetfile("package.json"))
if (!_json || !_json.Kei) {
  console.error("Unable to find valid Kei config, unable to register service application.")
  return
}

module.exports._Kei = {
  appId: null,
  instanceId: uuid.generate(),
  registryEndpoint: process.env.KEI_HOST
}

module.exports.cfg = cfgFactory.create(_json, this._Kei.instanceId)

const bootUp = async () => {
  try {
    console.log("cfg", this.cfg)
    const url = `${this._Kei.registryEndpoint}/applications`
    const r = await request.post(url, {
      body: this.cfg,
      headers: {
        "Content-Type": "application/javascript"
      },
      json: true,
    })

    this._Kei.appId = r.appId
    global._Kei = this._Kei

    console.log("Registered with Kei Jidosha at", this._Kei.registryEndpoint, this._Kei.appId, this._Kei.instanceId)
  } catch (err) {
    console.error("Error registering with Kei Jidosha at", this._Kei.registryEndpoint, err)
  }
}

const terminate = async (signal) => {
  console.log(`Application ${this._Kei.appId} stopping on ${signal} for instance ${this._Kei.instanceId}`)

  try {
    const url = `${this._Kei.registryEndpoint}/applications/${this._Kei.appId}/instances/${this._Kei.instanceId}`
    const r = await request.delete(url)

    console.log("Deregistered with Kei Jidosha for instanceid", this._Kei.appId)
  } catch (err) {
    console.log("Error deregistering from Kei Jidosha", err)
  }

  process.exit()
}

process.on('SIGINT', () => {
  terminate('SIGINT')
})

process.on('SIGTERM', () => {
  terminate('SIGTERM')
})

bootUp()


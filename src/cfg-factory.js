const os = require("os")

module.exports.create = (json, instanceId) => {
    return {
      application: {
        "name": json.name,
        "repository": json.repository,
        "author": json.author,
      },
      instance: {
        "id": instanceId,
        "version": json.version,
        "host": os.hostname()
      },
      ...json.Kei
    }
}

const fs = require("fs")
const path = require("path")

module.exports.tryGetfile = (filename) => {
  let file = null
  try {
    const appFile = path.resolve(require.main.filename)
    const appDir = appFile.substr(0,appFile.lastIndexOf("/"))
    file = fs.readFileSync(`${appDir}/${filename}`, "utf8")
  } catch (err) {
    if (err.code !== "ENOENT") {
      throw err
    }

    const appFile = path.resolve(path.join(require.main.filename, ".."))
    const appDir = appFile.substr(0, appFile.lastIndexOf("/"))

    file = fs.readFileSync(`${appDir}/${filename}`, "utf8")
  }

  return file
}
